# 🧳 Mini Wallet

## How To Install

### Clone The Repo
```BASH
$ git clone https://gitlab.com/liharsw/mini-wallet.git
```

### Module Download

```BASH
$ go mod download
```

### Run 

```BASH
$ go run .
```

`Then Open POSTMAN Mini Wallet Collection`



# 📖 ABOUT

[Mini Walet](https://gitlab.com/liharsw/mini-wallet) is a service for deposit and withdrawals virtual wallet