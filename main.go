package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	mw "gitlab.com/liharsw/mini-wallet/wallet"
)

func initWallet(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": "init wallet"})
}

func main() {
	router := gin.Default()

	v1 := router.Group("/api/v1")
	{
		v1.POST("/init", mw.InitWallet)
		v1.POST("/wallet", mw.EnableWallet)
		v1.GET("/wallet", mw.BalanceWallet)
		v1.POST("/wallet/deposits", mw.DepositWallet)
		v1.POST("/wallet/withdrawals", mw.WithdrawWallet)
		v1.PATCH("/wallet", mw.DisableWallet)
	}

	router.Run(":80")
}
