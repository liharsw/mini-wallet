package wallet

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func InitWallet(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"implement": "initWallet"})
}

func EnableWallet(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"implement": "enableWallet"})
}

func BalanceWallet(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"implement": "balanceWallet"})
}

func DepositWallet(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"implement": "depositWallet"})
}

func WithdrawWallet(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"implement": "withdrawWallet"})
}

func DisableWallet(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"implement": "disableWallet"})
}
